
from itertools import count
from operator import methodcaller
from typing import Counter
from flask import Flask
from flask.templating import render_template
from flask_restplus import Api, Resource, fields
import re
import datetime

from werkzeug.contrib.fixers import ProxyFix



import MySQLdb

db = MySQLdb.connect(host="localhost",    # your host, usually localhost
                     user="root",         # your username
                     passwd="madhu",  # your password
                     db="todo",
                     port=3307)        # name of the data base

# you must create a Cursor object. It will let
#  you execute all the queries you need
cur = db.cursor()


app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)


#wsgi_app -> used so that middlewares can be applied without losing a reference to the app object. 

#ProxyFix -> This middleware is applied to add HTTP proxy support to an application 
api = Api(app, version='1.0', title='TodoMVC API',
    description='A simple TodoMVC API',
)


ns = api.namespace('todos', description='TODO operations')

todo = api.model('Todo', {
    'id': fields.Integer(readonly=True, description='The task unique identifier'),
    'task': fields.String(required=True, description='The task details'),
    'due_date' : fields.Date(required=True, description='deadline in yyyy-mm-dd format'),
    'status' : fields.String(required=True, description='work status enter any one of the following: \'not started\', \'finished\', \'in progress\'')
})

user = api.model('User',{
    'id' : fields.String(readonly=True, description='Your email id'),
    'password' : fields.String(readonly=True, description='Your password')
})

def get_object(todo):
    tasks = []
    for each in todo:
        task = dict()
        task['id'] = each[0]
        task['task'] = each[1]
        task['due_date'] = each[2]
        task['status'] = each[3]
        tasks.append(task)
    return tasks

def get_id(id):
    cur.execute('SELECT * FROM  TODOLIST WHERE ID ='+str(id))
    todo = cur.fetchall()
    
    
    if(cur.rowcount==0):
        api.abort(404, "Todo {} doesn't exist".format(id))
        return
    todo = todo[0]
    task = dict()
    task['id'] = todo[0]
    task['task'] = todo[1]
    task['due_date'] = todo[2]
    task['status'] = todo[3]
    
    return task
        

def create(data):
    
    due = data['due_date']
    x = re.search("[0-9]{4}-[0-9]{2}-[0-9]{2}", due)
    if(x == None):
        api.abort(404, "Enter Due date in yyyy-mm-dd format")
        return
    if(data['status']!="not started" and data['status']!="finished" and data['status']!="in progress"):
        api.abort(404, "work status enter any one of the following: 'not started', 'finished', 'in progress'")
        return
    cur.execute('SELECT * FROM ids')
    counter = cur.fetchall() 
    counter= counter[0][0] +1
    print('inserting id: ',counter)
    cur.execute('UPDATE IDS SET ID'+'='+str(counter))
    db.commit()
    sql ='INSERT INTO TODOLIST VALUES (%s, %s, %s, %s)'
    val =(str(counter), data['task'], data['due_date'], data['status'])
    cur.execute(sql,val)
    db.commit()
    print(cur.rowcount)
    cur.execute('SELECT * FROM TODOLIST')
    todo = cur.fetchall()
    tasks = get_object(todo)

    return tasks

def get_date(date):
    cur.execute('SELECT * FROM  TODOLIST WHERE DUE_DATE = \''+ date + '\'')
    todo = cur.fetchall()
    if(cur.rowcount == 0):
        api.abort(404, "Todo with due date {} doesn't exist".format(date))
        return
    tasks =[]
    for each in todo:
        if(each[2]!=date):
            continue
        task = dict()
        task['id'] = each[0]
        task['task'] = each[1]
        task['due_date'] = each[2]
        task['status'] = each[3]
        tasks.append(task)
        print(task)
    return tasks



def get_todo():
    cur.execute('SELECT * FROM TODOLIST')
    todo = cur.fetchall()
    tasks = get_object(todo)
    return tasks

def get_overdue():
    cur.execute('SELECT * FROM TODOLIST')
    todo = cur.fetchall()
    tasks =[]
    today = datetime.datetime.now()
    date = str(today.date())
    for each in todo:
        if(each[2]<=date and each[3]!="finished"):
            task = dict()
            task['id'] = each[0]
            task['task'] = each[1]
            task['due_date'] = each[2]
            task['status'] = each[3]
            tasks.append(task)
    if(tasks):
        return tasks
    else:
        api.abort(404, "No work overdue")
        return


def get_finished():
    cur.execute('SELECT * FROM TODOLIST')
    todo = cur.fetchall()
    tasks =[]
    for each in todo:
        if(each[3]=="finished"):
            task = dict()
            task['id'] = each[0]
            task['task'] = each[1]
            task['due_date'] = each[2]
            task['status'] = each[3]
            tasks.append(task)
    
    if(tasks):
        return tasks
    else:
        api.abort(404, "No work has been completed yet")
        return


def delete_id(id):
    cur.execute('SELECT * FROM  TODOLIST WHERE ID ='+str(id))
    
    if(cur.rowcount==0):
        api.abort(404, "Todo {} doesn't exist".format(id))
        return
    cur.execute('DELETE FROM TODOLIST WHERE ID = '+str(id))
    db.commit()
    cur.execute('SELECT * FROM TODOLIST')
    todo = cur.fetchall()
    tasks =get_object(todo)
    return tasks
    

def update(data, id):
    cur.execute('SELECT * FROM  TODOLIST WHERE ID ='+str(id))
    print(data)
    if(cur.rowcount==0):
        api.abort(404, "Todo {} doesn't exist".format(id))
        return
    due = data['due_date']
    x = re.search("[0-9]{4}-[0-9]{2}-[0-9]{2}", due)
    if(x == None):
        api.abort(404, "Enter Due date in yyyy-mm-dd format")
        return
    if(data['status']!="not started" and data['status']!="finished" and data['status']!="in progress"):
        api.abort(404, "work status enter any one of the following: 'not started', 'finished', 'in progress'")
        return
    cur.execute('UPDATE TODOLIST SET TASK'+'=\''+str(data['task'])+'\', DUE_DATE=\''+str(data['due_date'])+'\', FINISHED=\''+str(data['status'])+'\' WHERE ID='+str(id)+';')
    db.commit()
    cur.execute('SELECT * FROM TODOLIST')
    todo = cur.fetchall()
    print(todo)
    tasks = get_object(todo)
        
    return tasks



@ns.route('/')
class TodoList(Resource):
    '''Shows a list of all todos, and lets you POST to add new tasks'''
    @ns.doc('list_todos')
    @ns.marshal_list_with(todo)
    def get(self):
        '''List all tasks'''
        return get_todo()

    @ns.doc('create_todo')
    @ns.expect(todo)
    @ns.marshal_with(todo, code=201)
    def post(self):
        '''Create a new task'''
        return create(api.payload), 201

@ns.route('/due/<string:due_date>')
@ns.response(404, 'Todo not found')
@ns.param('due_date', 'Deadline')
class Todo(Resource):
    @ns.doc('get_todo')
    @ns.marshal_with(todo)
    
    def get(self, due_date):
        return get_date(due_date)


@ns.route('/overdue')
class Todo(Resource):
    @ns.doc('get_todo')
    @ns.marshal_with(todo)
    def get(self):
        return get_overdue()


@ns.route('/finished')
class Todo(Resource):
    @ns.doc('get_todo')
    @ns.marshal_with(todo)
    def get(self):
        return get_finished()


@ns.route('/<int:id>')
@ns.response(404, 'Todo not found')
@ns.param('id', 'The task identifier')
class Todo(Resource):
    '''Show a single todo item and lets you delete them'''
    @ns.doc('get_todo')
    @ns.marshal_with(todo)
    def get(self, id):
        '''Fetch a given resource'''
        return get_id(id)

    @ns.doc('delete_todo')
    @ns.response(204, 'Todo deleted')
    def delete(self, id):
        '''Delete a task given its identifier'''
        return delete_id(id)
        

    @ns.expect(todo)
    @ns.marshal_with(todo)
    def put(self, id):
        '''Update a task given its identifier'''
        return update(api.payload, id)
        
        


if __name__ == '__main__':

    
    app.run(debug=True)