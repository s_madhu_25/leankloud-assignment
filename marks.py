#!/usr/bin/python

import sys

import pandas as pd # data processing

#Dataset
data = pd.read_csv(sys.argv[1])
#print('DATASET\n')
#print(data.head())
#print('\n\nDataset shape:',data.shape)
#print('\n\n dataset description')
#print(data.describe())

#print('\nfinding min and max using describe()')
#print('Highest mark in maths: ',data.describe()['Maths']['max'])
#print('Lowest mark in maths: ',data.describe()['Maths']['min'])
#print('class average in maths', data.describe()['Maths']['mean'])

def highest(subject):
    def find_max(arr):
        indices = []
        max_index= 0
        maximum = 0
        for i in range(len(arr)):
            if(arr[i]==maximum):
                indices.append(i)
            elif(arr[i]>maximum):
                maximum = arr[i]
                indices = [i]
        return indices
    marks = data[subject]
    ind = find_max(marks)
    print('\nTopper in ',subject, 'is: ',end="")
    for i in range(len(ind)):
        if(i==0):
            print(data['Name'][ind[i]], end="")
        else:
            print(", ",data['Name'][ind[i]], end="")



#print('\n\nAdding Total..')
data['Total'] = data['Maths'] + data['Biology'] + data['English'] + data['Physics'] + data['Chemistry'] + data['Hindi']

#print(data.head())

#print('\n\nAdding percentage...')
data['Percentage'] = data['Total']/5
print('dataset after adding total and percentage')
print(data.head())

data.describe()


def sort_by_percentage(arr, names):
    if(len(arr))>1:
        mid = len(arr)//2
        left = arr[:mid]
        right = arr[mid:]
        names_l = names[:mid]
        names_r = names[mid:]
        sort_by_percentage(left,names_l)
        sort_by_percentage(right, names_r)
        
        i=0
        j=0
        k=0
        
        while(i < len(left) and i<len(names_l) and j<len(right) and j<len(names_r)):
            if(left[i] > right[j]):
                arr[k] = left[i]
                names[k] = names_l[i]
                i+=1
            else:
                arr[k] = right[j]
                names[k] = names_r[j]
                j+=1
            k+=1
            
        while(i<len(left) and i<len(names_l)):
            arr[k] = left[i]
            names[k] = names_l[i]
            k+=1
            i+=1
        while(j<len(right) and j<len(names_r)):
            arr[k] = right[j]
            names[k] = names_r[j]
            j+=1
            k+=1

print('\n\n\nFinding toppers in subjects')

Subjects = ['Maths', 'Biology', 'English', 'Physics', 'Chemistry', 'Hindi']
for each in Subjects:
    highest(each)

arr = list(data['Percentage'])
names = list(data['Name'])

print('\n\nFinding Top 3 students in class\n')
sort_by_percentage(arr, names)

print('Best students in class are:', names[0], ',',names[1],',', names[2])


print('\n\n\nTime Complexity :')
    
print('\n        Function                  --       Complexity')
print('find_max(arr)                     --         O(n)')
print('highest(subject)                  --         O(n + val)')
print('sort_by_percentage(marks, names)  --         O(n log n)')

 
print('\n\nhere, n -> length of arr (30 in this case)')
print('      val -> number of toppers in a subject.') 

